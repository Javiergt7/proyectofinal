﻿using MVC_API05.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace MVC_05.Controllers
{
    public class PersonasController : Controller
    {
        // GET: Personas
        public ActionResult Index()
        {
            List<Persona> personas = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44329/api/");

                var response = client.GetAsync("personas");
                response.Wait();

                var result = response.Result;
                if (result.IsSuccessStatusCode)
                {
                    var reader = result.Content.ReadAsAsync<List<Persona>>();
                    reader.Wait();
                    personas = reader.Result;
                }
            }
            return View(personas);
        }

        // GET: Personas/Details/5
        public ActionResult Details(int id)
        {
            Persona persona = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44329/api/");

                var response = client.GetAsync("personas/" + id.ToString());
                response.Wait();

                var result = response.Result;
                if (result.IsSuccessStatusCode)
                {
                    var reader = result.Content.ReadAsAsync<Persona>();
                    reader.Wait();
                    persona = reader.Result;
                }
            }
            return View(persona);
        }

        // GET: Personas/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Personas/Create
        [HttpPost]
        public ActionResult Create(Persona persona)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44329/api/");

                //HTTP POST
                var postTask = client.PostAsJsonAsync<Persona>("personas", persona);
                postTask.Wait();

                var result = postTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");

                }

            }
            ModelState.AddModelError(string.Empty, "Server Error,Please contact Administrator.");

            return View();
        }

        // GET: Personas/Edit/5
        public ActionResult Edit(int id)
        {
            Persona persona = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44329/api/");

                var response = client.GetAsync("personas/" + id.ToString());
                response.Wait();

                var result = response.Result;
                if (result.IsSuccessStatusCode)
                {
                    var reader = result.Content.ReadAsAsync<Persona>();
                    reader.Wait();
                    persona = reader.Result;
                }
            }
            return View(persona);
        }

        // POST: Personas/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, Persona persona)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44329/api/");

                //HTTP PUT
                var putTask = client.PutAsJsonAsync<Persona>("personas/" + id.ToString(), persona);
                putTask.Wait();

                var result = putTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
            }

            ModelState.AddModelError(string.Empty, "Server Error. Please contact administrator.");

            return View(persona);
        }

        // GET: Personas/Delete/5
        public ActionResult Delete(int id)
        {
            Persona persona = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44329/api/");

                var response = client.GetAsync("personas/" + id.ToString());
                response.Wait();

                var result = response.Result;
                if (result.IsSuccessStatusCode)
                {
                    var reader = result.Content.ReadAsAsync<Persona>();
                    reader.Wait();
                    persona = reader.Result;
                }
            }
            return View(persona);
        }

        // POST: Personas/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, Persona persona)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44329/api/");

                //HTTP DELETE
                var deleteTask = client.DeleteAsync("personas/" + id.ToString());
                deleteTask.Wait();

                var result = deleteTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");

                }

            }
            ModelState.AddModelError(string.Empty, "Server Error,Please contact Administrator.");

            return View();
        }
    }
}
