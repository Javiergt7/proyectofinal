﻿using MVC_API05.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace MVC_05.Controllers
{
    public class DetalleCitasController : Controller
    {
        // GET: DetalleCitas
        public ActionResult Index()
        {
            List<DetalleCita> detalleCitas = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44329/api/");

                var response = client.GetAsync("detalleCitas");
                response.Wait();

                var result = response.Result;
                if (result.IsSuccessStatusCode)
                {
                    var reader = result.Content.ReadAsAsync<List<DetalleCita>>();
                    reader.Wait();
                    detalleCitas = reader.Result;
                }
            }
            return View(detalleCitas);
        }

        // GET: DetalleCitas/Details/5
        public ActionResult Details(int id)
        {
            DetalleCita detalleCita = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44329/api/");

                var response = client.GetAsync("detalleCitas/" + id.ToString());
                response.Wait();

                var result = response.Result;
                if (result.IsSuccessStatusCode)
                {
                    var reader = result.Content.ReadAsAsync<DetalleCita>();
                    reader.Wait();
                    detalleCita = reader.Result;
                }
            }
            return View(detalleCita);
        }

        // GET: DetalleCitas/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: DetalleCitas/Create
        [HttpPost]
        public ActionResult Create(DetalleCita detalleCita)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44329/api/");

                //HTTP POST
                var postTask = client.PostAsJsonAsync<DetalleCita>("detalleCitas", detalleCita);
                postTask.Wait();

                var result = postTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");

                }

            }
            ModelState.AddModelError(string.Empty, "Server Error,Please contact Administrator.");

            return View();
        }

        // GET: DetalleCitas/Edit/5
        public ActionResult Edit(int id)
        {
            DetalleCita detalleCita = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44329/api/");

                var response = client.GetAsync("detalleCitas/" + id.ToString());
                response.Wait();

                var result = response.Result;
                if (result.IsSuccessStatusCode)
                {
                    var reader = result.Content.ReadAsAsync<DetalleCita>();
                    reader.Wait();
                    detalleCita = reader.Result;
                }
            }
            return View(detalleCita);
        }

        // POST: DetalleCitas/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, DetalleCita detalleCita)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44329/api/");

                //HTTP PUT
                var putTask = client.PutAsJsonAsync<DetalleCita>("detalleCitas/" + id.ToString(), detalleCita);
                putTask.Wait();

                var result = putTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
            }

            ModelState.AddModelError(string.Empty, "Server Error. Please contact administrator.");

            return View(detalleCita);
        }

        // GET: DetalleCitas/Delete/5
        public ActionResult Delete(int id)
        {
            DetalleCita detalleCita = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44329/api/");

                var response = client.GetAsync("detalleCitas/" + id.ToString());
                response.Wait();

                var result = response.Result;
                if (result.IsSuccessStatusCode)
                {
                    var reader = result.Content.ReadAsAsync<DetalleCita>();
                    reader.Wait();
                    detalleCita = reader.Result;
                }
            }
            return View(detalleCita);
        }

        // POST: DetalleCitas/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, DetalleCita detalleCita)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44329/api/");

                //HTTP DELETE
                var deleteTask = client.DeleteAsync("detalleCitas/" + id.ToString());
                deleteTask.Wait();

                var result = deleteTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");

                }

            }
            ModelState.AddModelError(string.Empty, "Server Error,Please contact Administrator.");

            return View();
        }
    }
}
