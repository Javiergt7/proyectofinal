﻿using MVC_API05.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace MVC_05.Controllers
{
    public class HistoriaClinicasController : Controller
    {
        // GET: HistoriaClinicas
        public ActionResult Index()
        {
            List<HistoriaClinica> historiaClinicas = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44329/api/");

                var response = client.GetAsync("historiaClinicas");
                response.Wait();

                var result = response.Result;
                if (result.IsSuccessStatusCode)
                {
                    var reader = result.Content.ReadAsAsync<List<HistoriaClinica>>();
                    reader.Wait();
                    historiaClinicas = reader.Result;
                }
            }
            return View(historiaClinicas);
        }

        // GET: HistoriaClinicas/Details/5
        public ActionResult Details(int id)
        {
            HistoriaClinica historiaClinica = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44329/api/");

                var response = client.GetAsync("citas/" + id.ToString());
                response.Wait();

                var result = response.Result;
                if (result.IsSuccessStatusCode)
                {
                    var reader = result.Content.ReadAsAsync<HistoriaClinica>();
                    reader.Wait();
                    historiaClinica = reader.Result;
                }
            }
            return View(historiaClinica);
        }

        // GET: HistoriaClinicas/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: HistoriaClinicas/Create
        [HttpPost]
        public ActionResult Create(HistoriaClinica historiaClinica)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44329/api/");

                //HTTP POST
                var postTask = client.PostAsJsonAsync<HistoriaClinica>("historiaClinicas", historiaClinica);
                postTask.Wait();

                var result = postTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");

                }

            }
            ModelState.AddModelError(string.Empty, "Server Error,Please contact Administrator.");

            return View();
        }

        // GET: HistoriaClinicas/Edit/5
        public ActionResult Edit(int id)
        {
            HistoriaClinica historiaClinica = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44329/api/");

                var response = client.GetAsync("historiaClinica/" + id.ToString());
                response.Wait();

                var result = response.Result;
                if (result.IsSuccessStatusCode)
                {
                    var reader = result.Content.ReadAsAsync<HistoriaClinica>();
                    reader.Wait();
                    historiaClinica = reader.Result;
                }
            }
            return View(historiaClinica);
        }

        // POST: HistoriaClinicas/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, HistoriaClinica historiaClinica)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44329/api/");

                //HTTP PUT
                var putTask = client.PutAsJsonAsync<HistoriaClinica>("s/" + id.ToString(), historiaClinica);
                putTask.Wait();

                var result = putTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
            }

            ModelState.AddModelError(string.Empty, "Server Error. Please contact administrator.");

            return View(historiaClinica);
        }

        // GET: HistoriaClinicas/Delete/5
        public ActionResult Delete(int id)
        {
            HistoriaClinica historiaClinica = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44329/api/");

                var response = client.GetAsync("historiaClinicas/" + id.ToString());
                response.Wait();

                var result = response.Result;
                if (result.IsSuccessStatusCode)
                {
                    var reader = result.Content.ReadAsAsync<HistoriaClinica>();
                    reader.Wait();
                    historiaClinica = reader.Result;
                }
            }
            return View(historiaClinica);
        }

        // POST: HistoriaClinicas/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, HistoriaClinica historiaClinica)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44329/api/");

                //HTTP DELETE
                var deleteTask = client.DeleteAsync("historiaClinicas/" + id.ToString());
                deleteTask.Wait();

                var result = deleteTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");

                }

            }
            ModelState.AddModelError(string.Empty, "Server Error,Please contact Administrator.");

            return View();
        }
    }
}
