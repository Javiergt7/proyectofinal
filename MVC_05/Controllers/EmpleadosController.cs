﻿using MVC_API05.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace MVC_05.Controllers
{
    public class EmpleadosController : Controller
    {
        // GET: Empleados
        public ActionResult Index()
        {
            List<Empleado> empleados = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44329/api/");

                var response = client.GetAsync("empleados");
                response.Wait();

                var result = response.Result;
                if (result.IsSuccessStatusCode)
                {
                    var reader = result.Content.ReadAsAsync<List<Empleado>>();
                    reader.Wait();
                    empleados = reader.Result;
                }
            }
            return View(empleados);
        }

        // GET: Empleados/Details/5
        public ActionResult Details(int id)
        {
            Empleado empleado = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44329/api/");

                var response = client.GetAsync("empleados/" + id.ToString());
                response.Wait();

                var result = response.Result;
                if (result.IsSuccessStatusCode)
                {
                    var reader = result.Content.ReadAsAsync<Empleado>();
                    reader.Wait();
                    empleado = reader.Result;
                }
            }
            return View(empleado);
        }

        // GET: Empleados/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Empleados/Create
        [HttpPost]
        public ActionResult Create(Empleado empleado)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44329/api/");

                //HTTP POST
                var postTask = client.PostAsJsonAsync<Empleado>("empleados", empleado);
                postTask.Wait();

                var result = postTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");

                }

            }
            ModelState.AddModelError(string.Empty, "Server Error,Please contact Administrator.");

            return View();
        }

        // GET: Empleados/Edit/5
        public ActionResult Edit(int id)
        {
            Empleado empleado = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44329/api/");

                var response = client.GetAsync("empleados/" + id.ToString());
                response.Wait();

                var result = response.Result;
                if (result.IsSuccessStatusCode)
                {
                    var reader = result.Content.ReadAsAsync<Empleado>();
                    reader.Wait();
                    empleado = reader.Result;
                }
            }
            return View(empleado);
        }

        // POST: Empleados/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, Empleado empleado)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44329/api/");

                //HTTP PUT
                var putTask = client.PutAsJsonAsync<Empleado>("empleados/" + id.ToString(), empleado);
                putTask.Wait();

                var result = putTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
            }

            ModelState.AddModelError(string.Empty, "Server Error. Please contact administrator.");

            return View(empleado);
        }

        // GET: Empleados/Delete/5
        public ActionResult Delete(int id)
        {
            Empleado empleado = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44329/api/");

                var response = client.GetAsync("empleados/" + id.ToString());
                response.Wait();

                var result = response.Result;
                if (result.IsSuccessStatusCode)
                {
                    var reader = result.Content.ReadAsAsync<Empleado>();
                    reader.Wait();
                    empleado = reader.Result;
                }
            }
            return View(empleado);
        }

        // POST: Empleados/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, Empleado empleado)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44329/api/");

                //HTTP DELETE
                var deleteTask = client.DeleteAsync("empleados/" + id.ToString());
                deleteTask.Wait();

                var result = deleteTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");

                }

            }
            ModelState.AddModelError(string.Empty, "Server Error,Please contact Administrator.");

            return View();
        }
    }
}
