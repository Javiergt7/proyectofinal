﻿using MVC_API05.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace MVC_05.Controllers
{
    public class CatalogoTrabajosController : Controller
    {
        // GET: CatalogoTrabajos
        public ActionResult Index()
        {
            List<CatalogoTrabajo> catalogoTrabajos = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44329/api/");

                var response = client.GetAsync("catalogoTrabajos");
                response.Wait();

                var result = response.Result;
                if (result.IsSuccessStatusCode)
                {
                    var reader = result.Content.ReadAsAsync<List<CatalogoTrabajo>>();
                    reader.Wait();
                    catalogoTrabajos = reader.Result;
                }
            }
            return View(catalogoTrabajos);
        }

        // GET: CatalogoTrabajos/Details/5
        public ActionResult Details(int id)
        {
            CatalogoTrabajo catalogoTrabajo = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44329/api/");

                var response = client.GetAsync("catalogoTrabajos/" + id.ToString());
                response.Wait();

                var result = response.Result;
                if (result.IsSuccessStatusCode)
                {
                    var reader = result.Content.ReadAsAsync<CatalogoTrabajo>();
                    reader.Wait();
                    catalogoTrabajo = reader.Result;
                }
            }
            return View(catalogoTrabajo);
        }

        // GET: CatalogoTrabajos/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: CatalogoTrabajos/Create
        [HttpPost]
        public ActionResult Create(CatalogoTrabajo catalogoTrabajo)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44329/api/");

                //HTTP POST
                var postTask = client.PostAsJsonAsync<CatalogoTrabajo>("catalogoTrabajos", catalogoTrabajo);
                postTask.Wait();

                var result = postTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");

                }

            }
            ModelState.AddModelError(string.Empty, "Server Error,Please contact Administrator.");

            return View();
        }

        // GET: CatalogoTrabajos/Edit/5
        public ActionResult Edit(int id)
        {
            CatalogoTrabajo catalogoTrabajo = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44329/api/");

                var response = client.GetAsync("catalogoTrabajos/" + id.ToString());
                response.Wait();

                var result = response.Result;
                if (result.IsSuccessStatusCode)
                {
                    var reader = result.Content.ReadAsAsync<CatalogoTrabajo>();
                    reader.Wait();
                    catalogoTrabajo = reader.Result;
                }
            }
            return View(catalogoTrabajo);
        }

        // POST: CatalogoTrabajos/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, CatalogoTrabajo catalogoTrabajo)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44329/api/");

                //HTTP PUT
                var putTask = client.PutAsJsonAsync<CatalogoTrabajo>("CatalogoTrabajos/" + id.ToString(), catalogoTrabajo);
                putTask.Wait();

                var result = putTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
            }

            ModelState.AddModelError(string.Empty, "Server Error. Please contact administrator.");

            return View(catalogoTrabajo);
        }

        // GET: CatalogoTrabajos/Delete/5
        public ActionResult Delete(int id)
        {
            CatalogoTrabajo catalogoTrabajo = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44329/api/");

                var response = client.GetAsync("catalogoTrabajos/" + id.ToString());
                response.Wait();

                var result = response.Result;
                if (result.IsSuccessStatusCode)
                {
                    var reader = result.Content.ReadAsAsync<CatalogoTrabajo>();
                    reader.Wait();
                    catalogoTrabajo = reader.Result;
                }
            }
            return View(catalogoTrabajo);
        }

        // POST: CatalogoTrabajos/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, CatalogoTrabajo catalogoTrabajo)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44329/api/");

                //HTTP DELETE
                var deleteTask = client.DeleteAsync("CatalogoTrabajos/" + id.ToString());
                deleteTask.Wait();

                var result = deleteTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");

                }

            }
            ModelState.AddModelError(string.Empty, "Server Error,Please contact Administrator.");

            return View();
        }
    }
}
