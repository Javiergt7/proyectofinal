﻿using MVC_API05.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace MVC_05.Controllers
{
    public class CitasController : Controller
    {
        // GET: Citas
        public ActionResult Index()
        {
            List<Cita> citas = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44329/api/");

                var response = client.GetAsync("citas");
                response.Wait();

                var result = response.Result;
                if (result.IsSuccessStatusCode)
                {
                    var reader = result.Content.ReadAsAsync<List<Cita>>();
                    reader.Wait();
                    citas = reader.Result;
                }
            }
            return View(citas);
        }

        // GET: Citas/Details/5
        public ActionResult Details(int id)
        {
            Cita cita = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44329/api/");

                var response = client.GetAsync("citas/" + id.ToString());
                response.Wait();

                var result = response.Result;
                if (result.IsSuccessStatusCode)
                {
                    var reader = result.Content.ReadAsAsync<Cita>();
                    reader.Wait();
                    cita = reader.Result;
                }
            }
            return View(cita);
        }

        // GET: Citas/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Citas/Create
        [HttpPost]
        public ActionResult Create(Cita cita)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44329/api/");

                //HTTP POST
                var postTask = client.PostAsJsonAsync<Cita>("citas", cita);
                postTask.Wait();

                var result = postTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");

                }

            }
            ModelState.AddModelError(string.Empty, "Server Error,Please contact Administrator.");

            return View();
        }

        // GET: Citas/Edit/5
        public ActionResult Edit(int id)
        {
            Cita cita = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44329/api/");

                var response = client.GetAsync("citas/" + id.ToString());
                response.Wait();

                var result = response.Result;
                if (result.IsSuccessStatusCode)
                {
                    var reader = result.Content.ReadAsAsync<Cita>();
                    reader.Wait();
                    cita = reader.Result;
                }
            }
            return View(cita);
        }

        // POST: Citas/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, Cita cita)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44329/api/");

                //HTTP PUT
                var putTask = client.PutAsJsonAsync<Cita>("citas/" + id.ToString(), cita);
                putTask.Wait();

                var result = putTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
            }

            ModelState.AddModelError(string.Empty, "Server Error. Please contact administrator.");

            return View(cita);
        }

        // GET: Citas/Delete/5
        public ActionResult Delete(int id)
        {
            Cita cita = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44329/api/");

                var response = client.GetAsync("citas/" + id.ToString());
                response.Wait();

                var result = response.Result;
                if (result.IsSuccessStatusCode)
                {
                    var reader = result.Content.ReadAsAsync<Cita>();
                    reader.Wait();
                    cita = reader.Result;
                }
            }
            return View(cita);
        }

        // POST: Citas/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, Cita cita)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44329/api/");

                //HTTP DELETE
                var deleteTask = client.DeleteAsync("citas/" + id.ToString());
                deleteTask.Wait();

                var result = deleteTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");

                }

            }
            ModelState.AddModelError(string.Empty, "Server Error,Please contact Administrator.");

            return View();
        }
    }
}
