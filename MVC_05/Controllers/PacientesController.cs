﻿using MVC_API05.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace MVC_05.Controllers
{
    public class PacientesController : Controller
    {
        // GET: Pacientes
        public ActionResult Index()
        {
            List<Paciente> pacientes = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44329/api/");

                var response = client.GetAsync("pacientes");
                response.Wait();

                var result = response.Result;
                if (result.IsSuccessStatusCode)
                {
                    var reader = result.Content.ReadAsAsync<List<Paciente>>();
                    reader.Wait();
                    pacientes = reader.Result;
                }
            }
            return View(pacientes);
        }

        // GET: Pacientes/Details/5
        public ActionResult Details(int id)
        {
            Paciente paciente = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44329/api/");

                var response = client.GetAsync("pacientes/" + id.ToString());
                response.Wait();

                var result = response.Result;
                if (result.IsSuccessStatusCode)
                {
                    var reader = result.Content.ReadAsAsync<Paciente>();
                    reader.Wait();
                    paciente = reader.Result;
                }
            }
            return View(paciente);
        }

        // GET: Pacientes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Pacientes/Create
        [HttpPost]
        public ActionResult Create(Paciente paciente)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44329/api/");

                //HTTP POST
                var postTask = client.PostAsJsonAsync<Paciente>("pacientes", paciente);
                postTask.Wait();

                var result = postTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");

                }

            }
            ModelState.AddModelError(string.Empty, "Server Error,Please contact Administrator.");

            return View();
        }

        // GET: Pacientes/Edit/5
        public ActionResult Edit(int id)
        {
            Paciente paciente = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44329/api/");

                var response = client.GetAsync("pacientes/" + id.ToString());
                response.Wait();

                var result = response.Result;
                if (result.IsSuccessStatusCode)
                {
                    var reader = result.Content.ReadAsAsync<Paciente>();
                    reader.Wait();
                    paciente = reader.Result;
                }
            }
            return View(paciente);
        }

        // POST: Pacientes/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, Paciente paciente)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44329/api/");

                //HTTP PUT
                var putTask = client.PutAsJsonAsync<Paciente>("pacientes/" + id.ToString(), paciente);
                putTask.Wait();

                var result = putTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
            }

            ModelState.AddModelError(string.Empty, "Server Error. Please contact administrator.");

            return View(paciente);
        }

        // GET: Pacientes/Delete/5
        public ActionResult Delete(int id)
        {
            Paciente paciente = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44329/api/");

                var response = client.GetAsync("pacientes/" + id.ToString());
                response.Wait();

                var result = response.Result;
                if (result.IsSuccessStatusCode)
                {
                    var reader = result.Content.ReadAsAsync<Paciente>();
                    reader.Wait();
                    paciente = reader.Result;
                }
            }
            return View(paciente);
        }

        // POST: Pacientes/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, Paciente paciente)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44329/api/");

                //HTTP DELETE
                var deleteTask = client.DeleteAsync("pacientes/" + id.ToString());
                deleteTask.Wait();

                var result = deleteTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");

                }

            }
            ModelState.AddModelError(string.Empty, "Server Error,Please contact Administrator.");

            return View();
        }
    }
}
