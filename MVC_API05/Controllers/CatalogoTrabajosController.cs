﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using MVC_API05.Models;

namespace MVC_API05.Controllers
{
    public class CatalogoTrabajosController : ApiController
    {
        private Mi_BD1Entities db = new Mi_BD1Entities();

        // GET: api/CatalogoTrabajos
        public IQueryable<CatalogoTrabajo> GetCatalogoTrabajo()
        {
            return db.CatalogoTrabajo;
        }

        // GET: api/CatalogoTrabajos/5
        [ResponseType(typeof(CatalogoTrabajo))]
        public IHttpActionResult GetCatalogoTrabajo(int id)
        {
            CatalogoTrabajo catalogoTrabajo = db.CatalogoTrabajo.Find(id);
            if (catalogoTrabajo == null)
            {
                return NotFound();
            }

            return Ok(catalogoTrabajo);
        }

        // PUT: api/CatalogoTrabajos/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutCatalogoTrabajo(int id, CatalogoTrabajo catalogoTrabajo)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != catalogoTrabajo.Id)
            {
                return BadRequest();
            }

            db.Entry(catalogoTrabajo).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CatalogoTrabajoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/CatalogoTrabajos
        [ResponseType(typeof(CatalogoTrabajo))]
        public IHttpActionResult PostCatalogoTrabajo(CatalogoTrabajo catalogoTrabajo)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.CatalogoTrabajo.Add(catalogoTrabajo);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = catalogoTrabajo.Id }, catalogoTrabajo);
        }

        // DELETE: api/CatalogoTrabajos/5
        [ResponseType(typeof(CatalogoTrabajo))]
        public IHttpActionResult DeleteCatalogoTrabajo(int id)
        {
            CatalogoTrabajo catalogoTrabajo = db.CatalogoTrabajo.Find(id);
            if (catalogoTrabajo == null)
            {
                return NotFound();
            }

            db.CatalogoTrabajo.Remove(catalogoTrabajo);
            db.SaveChanges();

            return Ok(catalogoTrabajo);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CatalogoTrabajoExists(int id)
        {
            return db.CatalogoTrabajo.Count(e => e.Id == id) > 0;
        }
    }
}