﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using MVC_API05.Models;

namespace MVC_API05.Controllers
{
    public class DetalleCitasController : ApiController
    {
        private Mi_BD1Entities db = new Mi_BD1Entities();

        // GET: api/DetalleCitas
        public IQueryable<DetalleCita> GetDetalleCita()
        {
            return db.DetalleCita;
        }

        // GET: api/DetalleCitas/5
        [ResponseType(typeof(DetalleCita))]
        public IHttpActionResult GetDetalleCita(int id)
        {
            DetalleCita detalleCita = db.DetalleCita.Find(id);
            if (detalleCita == null)
            {
                return NotFound();
            }

            return Ok(detalleCita);
        }

        // PUT: api/DetalleCitas/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutDetalleCita(int id, DetalleCita detalleCita)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != detalleCita.Id)
            {
                return BadRequest();
            }

            db.Entry(detalleCita).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DetalleCitaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/DetalleCitas
        [ResponseType(typeof(DetalleCita))]
        public IHttpActionResult PostDetalleCita(DetalleCita detalleCita)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.DetalleCita.Add(detalleCita);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = detalleCita.Id }, detalleCita);
        }

        // DELETE: api/DetalleCitas/5
        [ResponseType(typeof(DetalleCita))]
        public IHttpActionResult DeleteDetalleCita(int id)
        {
            DetalleCita detalleCita = db.DetalleCita.Find(id);
            if (detalleCita == null)
            {
                return NotFound();
            }

            db.DetalleCita.Remove(detalleCita);
            db.SaveChanges();

            return Ok(detalleCita);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool DetalleCitaExists(int id)
        {
            return db.DetalleCita.Count(e => e.Id == id) > 0;
        }
    }
}